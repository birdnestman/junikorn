defmodule FrontendWeb.MainstageChController do
  use FrontendWeb, :controller
  alias Phoenix.LiveView

  def index(conn, _params) do
    LiveView.Controller.live_render(conn, FrontendWeb.MainstageChView, session: %{})
  end
end
