defmodule FrontendWeb.MainstageDeController do
  use FrontendWeb, :controller
  alias Phoenix.LiveView

  def index(conn, _params) do
    LiveView.Controller.live_render(conn, FrontendWeb.MainstageDeView, session: %{})
  end
end
