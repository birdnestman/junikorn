defmodule FrontendWeb.SavedView do
  use Phoenix.LiveView

  def render(assigns) do
    FrontendWeb.PageView.render("saved.html", assigns)
  end

  def mount(session, socket) do
    {:ok, assign(socket, names_de: Map.get(session, :names_de))}
  end
end
