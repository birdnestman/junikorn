defmodule FrontendWeb.MainstageDeView do
  use Phoenix.LiveView

  @topic "mainstage_de"

  def render(assigns) do
    FrontendWeb.PageView.render("mainstage.html", assigns)
  end

  def mount(_session, socket) do
    FrontendWeb.Endpoint.subscribe(@topic)

    {:ok, name} = JunikornEngine.Server.get_state(JunikornEngine.Server)

    {:ok, assign(socket, %{name_object: name})}
  end

  def handle_event("generate_name", _value, socket) do
    {:noreply, assign(socket, deploy_step: "start")}
  end

  def handle_info(%{topic: @topic, payload: state}, socket) do
    {:noreply, assign(socket, %{name_object: state})}
  end
end
