defmodule JunikornEngineTest.WordTest do
  use ExUnit.Case

  alias JunikornEngine.Word

  test "Get list of adjectives" do
    adjective = Word.get_adjectives(:de) |> List.first()
    assert %{m: _m, f: _f, u: _u} = adjective
  end

  test "Get list of nouns" do
    noun = Word.get_nouns(:de) |> List.first()
    assert %{word: _word, gender: _gender} = noun
  end

  test "Get random noun" do
    element =
      Word.get_random(Word.get_nouns(:de), :noun, :de)
      |> List.first()

    assert %{gender: _gender, word: _word} = element
  end

  test "Get random adjective" do
    words = [
      %{f: "schöne", m: "schöner", u: "schönes"},
      %{f: "verkleinerte", m: "verkleinerter", u: "verkleinertes"},
      %{f: "heisse", m: "heisser", u: "heisses"}
    ]

    element = Word.get_random(words, :adjective, :de)

    assert [%{word: _adjective} | words] = element
  end

  describe("transform_adjective/2") do
    test "Male" do
      word = Word.transform_adjective([%{gender: "m", word: "Fisch"}], %{m: "nasser", f: "nasse"})

      assert [%{word: "nasser"}] = word
    end

    test "Female" do
      word =
        Word.transform_adjective([%{gender: "f", word: "Kutsche"}], %{m: "nasser", f: "nasse"})

      assert [%{word: "nasse"}] = word
    end
  end
end
