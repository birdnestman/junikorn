defmodule JunikornEngineTest.NameTest do
  use ExUnit.Case

  alias JunikornEngine.Name

  describe "join\1" do
    test "join 3 words" do
      words = [
        %{
          word: "verkümmerter"
        },
        %{gender: "m", word: "Bunker"},
        %{gender: "m", word: "Fisch"}
      ]

      assert %{name: "verkümmerter Bunkerfisch", words: words} == Name.join(words)
    end

    test "join 2 words" do
      words = [
        %{gender: "m", word: "Bunker"},
        %{gender: "m", word: "Fisch"}
      ]

      assert %{name: "Bunkerfisch", words: words} == Name.join(words)
    end
  end

  describe "concat" do
    test "first word" do
      words = [
        %{
          word: "verkümmerter"
        },
        %{gender: "m", word: "Bunker"},
        %{gender: "m", word: "Fisch"}
      ]

      object = %{name: "", words: words}

      assert Name.concat(object, Enum.fetch!(words, 0)) == %{
               name: " verkümmerter",
               words: [
                 %{word: "verkümmerter"},
                 %{gender: "m", word: "Bunker"},
                 %{gender: "m", word: "Fisch"}
               ]
             }
    end

    test "secondlast word" do
      words = [
        %{
          word: "verkümmerter"
        },
        %{gender: "m", word: "Bunker"},
        %{gender: "m", word: "Fisch"}
      ]

      object = %{name: "", words: words}

      assert Name.concat(object, Enum.fetch!(words, 1), :secondlast) == %{
               name: " Bunker",
               words: [
                 %{word: "verkümmerter"},
                 %{gender: "m", word: "Bunker"},
                 %{gender: "m", word: "Fisch"}
               ]
             }
    end

    test "last word" do
      words = [
        %{
          word: "verkümmerter"
        },
        %{gender: "m", word: "Bunker"},
        %{gender: "m", word: "Fisch"}
      ]

      object = %{name: "", words: words}

      assert Name.concat(object, Enum.fetch!(words, 2), :last) == %{
               name: "fisch",
               words: [
                 %{word: "verkümmerter"},
                 %{gender: "m", word: "Bunker"},
                 %{gender: "m", word: "Fisch"}
               ]
             }
    end
  end
end
