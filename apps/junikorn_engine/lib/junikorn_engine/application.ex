defmodule JunikornEngine.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      JunikornEngine.Server,
      Supervisor.child_spec({JunikornEngine.NameGenerator, :de}, id: :generator_de),
      Supervisor.child_spec({JunikornEngine.Dictionary, :de}, id: :dictionary_de)
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: JunikornEngine.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
