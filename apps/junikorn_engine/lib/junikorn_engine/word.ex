defmodule JunikornEngine.Word do
  @moduledoc """
  Loads and transform words.
  """

  alias __MODULE__

  def get_nouns(locale \\ :de), do: Agent.get(locale, fn state -> state end).nouns

  def get_adjectives(locale \\ :de), do: Agent.get(locale, fn state -> state end).adjectives

  def get_random(container, :noun, locale),
    do: Enum.concat(container, Enum.take_random(get_nouns(locale), 2))

  @doc """
  Get randomly 0 or 1 adjectives from a list.
  """
  def get_random(container, :adjective, locale) do
    adjective_map =
      Enum.take_random(get_adjectives(locale), 1)
      |> List.first()

    if adjective_map != nil do
      transform_adjective(container, adjective_map) ++ container
    else
      container
    end
  end

  @doc """
  Get the word for a specific gender from an adjective and returns as a map.
  """
  def transform_adjective(nouns, adjective) do
    gender = define_gender(nouns)
    adjective = Map.get(adjective, String.to_atom(gender))
    [%{:word => adjective}]
  end

  def define_gender(nouns) do
    nouns
    |> List.last()
    |> Map.get(:gender)
  end

  def adapt_rules do
  end
end
