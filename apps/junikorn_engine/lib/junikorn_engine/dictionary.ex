defmodule JunikornEngine.Dictionary do
  @moduledoc """
  A dictionary contains words.
  """

  use Agent

  @doc """
  Starts a new dictionary.
  """
  def start_link(name) do
    Agent.start_link(fn -> %{nouns: nouns(name), adjectives: adjectives(name)} end,
      id: name,
      name: name
    )
  end

  @doc """
  Gets a value from the `dictionary` by `key`.
  """
  def get(dictionary, key) do
    Agent.get(dictionary, &Map.get(&1, key))
  end

  @doc """
  Puts the `value` for the given `key` in the `dictionary`.
  """
  def put(dictionary, key, value) do
    Agent.update(dictionary, &Map.put(&1, key, value))
  end

  def nouns(:de) do
    [
      %{
        gender: "m",
        word: "Fisch"
      },
      %{
        gender: "m",
        word: "Kleister"
      },
      %{
        gender: "u",
        word: "Einhorn"
      },
      %{
        gender: "m",
        word: "Hund"
      },
      %{
        gender: "m",
        word: "Kropf"
      },
      %{
        gender: "f",
        word: "Blume"
      },
      %{
        gender: "f",
        word: "Sonne"
      },
      %{
        gender: "f",
        word: "Drüse"
      },
      %{
        gender: "m",
        word: "Pinsel"
      },
      %{
        gender: "m",
        word: "Topf"
      },
      %{
        gender: "m",
        word: "Arm"
      },
      %{
        gender: "u",
        word: "Gesicht"
      },
      %{
        gender: "m",
        word: "Schlauch"
      },
      %{
        gender: "m",
        word: "Draht"
      },
      %{
        gender: "m",
        word: "Mensch"
      },
      %{
        gender: "m",
        word: "Mann"
      },
      %{
        gender: "m",
        word: "Wirbel"
      },
      %{
        gender: "u",
        word: "Knie"
      },
      %{
        gender: "u",
        word: "Geschwür"
      },
      %{
        gender: "m",
        word: "Keim"
      },
      %{
        gender: "m",
        word: "Schweiss"
      },
      %{
        gender: "m",
        word: "Samen"
      },
      %{
        gender: "m",
        word: "Eiter"
      },
      %{
        gender: "m",
        word: "Schleim"
      },
      %{
        gender: "m",
        word: "Parasit"
      },
      %{
        gender: "m",
        word: "Pilz"
      },
      %{
        gender: "m",
        word: "Knospe"
      },
      %{
        gender: "f",
        word: "Spore"
      },
      %{
        gender: "f",
        word: "Nutte"
      },
      %{
        gender: "f",
        word: "Banane"
      },
      %{
        gender: "u",
        word: "Hirn"
      },
      %{
        gender: "f",
        word: "Gurke"
      },
      %{
        gender: "m",
        word: "Flug"
      },
      %{
        gender: "m",
        word: "Penis"
      },
      %{
        gender: "m",
        word: "Stuhl"
      },
      %{
        gender: "f",
        word: "Wurst"
      },
      %{
        gender: "f",
        word: "Katze"
      },
      %{
        gender: "m",
        word: "Wurm"
      },
      %{
        gender: "m",
        word: "Schädel"
      },
      %{
        gender: "u",
        word: "Holz"
      },
      %{
        gender: "m",
        word: "Finger"
      },
      %{
        gender: "m",
        word: "Hut"
      },
      %{
        gender: "m",
        word: "Staub"
      },
      %{
        gender: "m",
        word: "Beutel"
      },
      %{
        gender: "m",
        word: "Gesang"
      },
      %{
        gender: "m",
        word: "Saft"
      },
      %{
        gender: "f",
        word: "Prothese"
      },
      %{
        gender: "u",
        word: "Tier"
      },
      %{
        gender: "m",
        word: "Stein"
      },
      %{
        gender: "f",
        word: "Niere"
      },
      %{
        gender: "m",
        word: "Apfel"
      },
      %{
        gender: "u",
        word: "Eis"
      },
      %{
        gender: "f",
        word: "Wüste"
      },
      %{
        gender: "m",
        word: "Aal"
      },
      %{
        gender: "m",
        word: "Kratzer"
      },
      %{
        gender: "m",
        word: "Hoden"
      },
      %{
        gender: "m",
        word: "Sack"
      },
      %{
        gender: "u",
        word: "Horn"
      },
      %{
        gender: "u",
        word: "Pulver"
      },
      %{
        gender: "u",
        word: "Puder"
      },
      %{
        gender: "u",
        word: "Messer"
      },
      %{
        gender: "f",
        word: "Droge"
      },
      %{
        gender: "m",
        word: "Busen"
      },
      %{
        gender: "m",
        word: "Leim"
      },
      %{
        gender: "m",
        word: "Bunker"
      },
      %{
        gender: "m",
        word: "Pflock"
      },
      %{
        gender: "m",
        word: "Bolzen"
      },
      %{
        gender: "m",
        word: "Pelz"
      },
      %{
        gender: "m",
        word: "Clown"
      },
      %{
        gender: "f",
        word: "Spitze"
      },
      %{
        gender: "u",
        word: "Gelenk"
      },
      %{
        gender: "f",
        word: "Ameise"
      },
      %{
        gender: "m",
        word: "Berg"
      },
      %{
        gender: "m",
        word: "Bauch"
      },
      %{
        gender: "m",
        word: "Baum"
      },
      %{
        gender: "m",
        word: "Muskel"
      },
      %{
        gender: "m",
        word: "Nabel"
      },
      %{
        gender: "m",
        word: "Rumpf"
      },
      %{
        gender: "u",
        word: "Monster"
      },
      %{
        gender: "m",
        word: "Kot"
      },
      %{
        gender: "f",
        word: "Kotze"
      },
      %{
        gender: "f",
        word: "Rakete"
      },
      %{
        gender: "m",
        word: "Kiosk"
      },
      %{
        gender: "m",
        word: "Arsch"
      },
      %{
        gender: "u",
        word: "Baby"
      },
      %{
        gender: "f",
        word: "Hose"
      },
      %{
        gender: "m",
        word: "Traum"
      },
      %{
        gender: "f",
        word: "Palme"
      },
      %{
        gender: "m",
        word: "Sex"
      },
      %{
        gender: "u",
        word: "Haar"
      },
      %{
        gender: "f",
        word: "Toilette"
      },
      %{
        gender: "f",
        word: "Tomate"
      },
      %{
        gender: "m",
        word: "Pfeffer"
      },
      %{
        gender: "u",
        word: "Schnitzel"
      },
      %{
        gender: "f",
        word: "Brosche"
      },
      %{
        gender: "f",
        word: "Katastrophe"
      },
      %{
        gender: "u",
        word: "Wasser"
      },
      %{
        gender: "f",
        word: "Leiche"
      },
      %{
        gender: "m",
        word: "Strahl"
      },
      %{
        gender: "m",
        word: "Puls"
      },
      %{
        gender: "u",
        word: "Risotto"
      },
      %{
        gender: "f",
        word: "Suppe"
      },
      %{
        gender: "f",
        word: "Wulst"
      },
      %{
        gender: "m",
        word: "Schwanz"
      },
      %{
        gender: "f",
        word: "Krüke"
      },
      %{
        gender: "m",
        word: "Zwerg"
      },
      %{
        gender: "f",
        word: "Runzel"
      },
      %{
        gender: "m",
        word: "Feger"
      },
      %{
        gender: "m",
        word: "Witz"
      },
      %{
        gender: "f",
        word: "Dose"
      },
      %{
        gender: "m",
        word: "Ring"
      },
      %{
        gender: "u",
        word: "Koks"
      }
    ]
  end

  def adjectives(:de) do
    [
      %{
        m: "schöner",
        f: "schöne",
        u: "schönes"
      },
      %{
        m: "verkleinerter",
        f: "verkleinerte",
        u: "verkleinertes"
      },
      %{
        m: "heisser",
        f: "heisse",
        u: "heisses"
      },
      %{
        m: "kurzer",
        f: "kurze",
        u: "kurzes"
      },
      %{
        m: "blauer",
        f: "blaue",
        u: "blaues"
      },
      %{
        m: "verkümmerter",
        f: "verkümmerte",
        u: "verkümmertes"
      },
      %{
        m: "armer",
        f: "arme",
        u: "armes"
      },
      %{
        m: "schleimiger",
        f: "schleimige",
        u: "schleimiges"
      },
      %{
        m: "drahtiger",
        f: "drahtige",
        u: "drahtiges"
      },
      %{
        m: "dicker",
        f: "dicke",
        u: "dickes"
      },
      %{
        m: "dünner",
        f: "dünne",
        u: "dünnes"
      },
      %{
        m: "runder",
        f: "runde",
        u: "rundes"
      },
      %{
        m: "unbeholfener",
        f: "unbeholfene",
        u: "unbeholfenes"
      },
      %{
        m: "selbständiger",
        f: "selbständige",
        u: "selbständiges"
      },
      %{
        m: "kastrierter",
        f: "kastrierte",
        u: "kastriertes"
      },
      %{
        m: "abgestorbener",
        f: "abgestorbene",
        u: "abgestorbenes"
      },
      %{
        m: "vergilbter",
        f: "vergilbte",
        u: "vergilbtes"
      },
      %{
        m: "brauner",
        f: "braune",
        f: "braunes"
      },
      %{
        m: "gehörtner",
        f: "gehörnte",
        u: "gehörntes"
      },
      %{
        m: "rasierter",
        f: "rasierte",
        u: "rasiertes"
      },
      %{
        m: "verkeilter",
        f: "verkeilte",
        u: "verkeiltes"
      },
      %{
        m: "chemischer",
        f: "chemische",
        u: "chemisches"
      },
      %{
        m: "klebriger",
        f: "klebrige",
        u: "klebriges"
      },
      %{
        m: "verklebter",
        f: "verklebte",
        u: "verkletes"
      },
      %{
        m: "pelziger",
        f: "pelzige",
        u: "pelziges"
      },
      %{
        m: "komischer",
        f: "komische",
        u: "komisches"
      },
      %{
        m: "gerader",
        f: "gerade",
        u: "gerades"
      },
      %{
        m: "krummer",
        f: "krumme",
        u: "krummes"
      },
      %{
        m: "zitteriger",
        f: "zitterige",
        u: "zitteriges"
      },
      %{
        m: "enthaltsamer",
        f: "enthaltsame",
        u: "enthaltsames"
      },
      %{
        m: "potenter",
        f: "potente",
        u: "potentes"
      },
      %{
        m: "vergährter",
        f: "vergährte",
        u: "vergährtes"
      },
      %{
        m: "zarter",
        f: "zarte",
        u: "zartes"
      },
      %{
        m: "tropfender",
        f: "tropfende",
        u: "tropfendes"
      },
      %{
        m: "gigantischer",
        f: "gigantische",
        u: "gigantisches"
      },
      %{
        m: "eiternder",
        f: "eiternde",
        u: "eiterndes"
      },
      %{
        m: "eifriger",
        f: "eifrige",
        u: "eifriges"
      },
      %{
        m: "hinkender",
        f: "hinkende",
        u: "hinkendes"
      },
      %{
        m: "niedlicher",
        f: "niedliche",
        u: "niedliches"
      },
      %{
        m: "höflicher",
        f: "höfliche",
        u: "höfliches"
      },
      %{
        m: "toter",
        f: "tote",
        u: "totes"
      },
      %{
        m: "betrunkener",
        f: "betrunkene",
        u: "betrunkenes"
      },
      %{
        m: "runzeliger",
        f: "runzelige",
        u: "runzeliges"
      },
      %{
        m: "sensibler",
        f: "sensible",
        u: "sensibles"
      },
      %{
        m: "katastrophaler",
        f: "katastrophale",
        u: "katastrophales"
      },
      %{
        m: "gieriger",
        f: "gierige",
        u: "gieriges"
      },
      %{
        m: "ausgeprägter",
        f: "ausgeprägte",
        u: "ausgeprägtes"
      },
      %{
        m: "dominanter",
        f: "dominante",
        u: "dominantes"
      },
      %{
        m: "unaufhaltbarer",
        f: "unaufhaltbare",
        u: "unaufhaltbares"
      },
      %{
        m: "berüchtiger",
        f: "berüchtige",
        u: "berüchtiges"
      },
      %{
        m: "pulsierender",
        f: "pulsierende",
        u: "pulsierendes"
      },
      %{
        m: "kochender",
        f: "kochende",
        u: "kochendes"
      },
      %{
        m: "pochender",
        f: "pochende",
        u: "pochendes"
      },
      %{
        m: "hungriger",
        f: "hungrige",
        u: "hungriges"
      },
      %{
        m: "kratzender",
        f: "kratzende",
        u: "kratzendes"
      },
      %{
        m: "flauschiger",
        f: "flauschige",
        u: "flauschiges"
      },
      %{
        m: "verbeulter",
        f: "verbeulte",
        u: "verbeultes"
      },
      %{
        m: "gehörnter",
        f: "gehörnte",
        u: "gehörntes"
      },
      %{
        m: "einbeiniger",
        f: "einbeinige",
        u: "einbeiniges"
      },
      %{
        m: "hoffnungsloser",
        f: "hoffnungslose",
        u: "hoffnungsloses"
      },
      %{
        m: "saftiger",
        f: "saftige",
        u: "saftiges"
      },
      %{
        m: "schmieriger",
        f: "schmierige",
        u: "schmieriges"
      }
    ]
  end

  def nouns(:ch) do
    [
      %{
        gender: "m",
        word: "Fisch"
      },
      %{
        gender: "m",
        word: "Chleister"
      },
      %{
        gender: "u",
        word: "Einhorn"
      },
      %{
        gender: "m",
        word: "Hund"
      },
      %{
        gender: "m",
        word: "Chropf"
      },
      %{
        gender: "f",
        word: "Blume"
      },
      %{
        gender: "f",
        word: "Sonnä"
      },
      %{
        gender: "f",
        word: "Drüsä"
      },
      %{
        gender: "m",
        word: "Pinsel"
      },
      %{
        gender: "m",
        word: "Topf"
      },
      %{
        gender: "m",
        word: "Arm"
      },
      %{
        gender: "u",
        word: "Gsicht"
      },
      %{
        gender: "m",
        word: "Schluuch"
      },
      %{
        gender: "m",
        word: "Draht"
      },
      %{
        gender: "m",
        word: "Mönsch"
      },
      %{
        gender: "m",
        word: "Ma"
      },
      %{
        gender: "m",
        word: "Wirbel"
      },
      %{
        gender: "m",
        word: "Chnü"
      },
      %{
        gender: "u",
        word: "Gschwüür"
      },
      %{
        gender: "m",
        word: "Keim"
      },
      %{
        gender: "m",
        word: "Schweiss"
      },
      %{
        gender: "m",
        word: "Same"
      },
      %{
        gender: "m",
        word: "Äiter"
      },
      %{
        gender: "m",
        word: "Schliim"
      },
      %{
        gender: "m",
        word: "Parasit"
      },
      %{
        gender: "m",
        word: "Pilz"
      },
      %{
        gender: "m",
        word: "Chnospe"
      },
      %{
        gender: "f",
        word: "Spore"
      },
      %{
        gender: "f",
        word: "Nutte"
      },
      %{
        gender: "f",
        word: "Bananä"
      },
      %{
        gender: "u",
        word: "Hirn"
      },
      %{
        gender: "f",
        word: "Gorke"
      },
      %{
        gender: "m",
        word: "Flug"
      },
      %{
        gender: "m",
        word: "Schnäbi"
      },
      %{
        gender: "m",
        word: "Stuel"
      },
      %{
        gender: "f",
        word: "Worscht"
      },
      %{
        gender: "m",
        word: "Worm"
      },
      %{
        gender: "m",
        word: "Schädel"
      },
      %{
        gender: "u",
        word: "Holz"
      },
      %{
        gender: "m",
        word: "Finger"
      },
      %{
        gender: "m",
        word: "Huät"
      },
      %{
        gender: "m",
        word: "Staub"
      },
      %{
        gender: "m",
        word: "Bütel"
      },
      %{
        gender: "m",
        word: "Gsang"
      },
      %{
        gender: "m",
        word: "Saft"
      },
      %{
        gender: "m",
        word: "Prothesä"
      },
      %{
        gender: "u",
        word: "Tier"
      },
      %{
        gender: "m",
        word: "Stäi"
      },
      %{
        gender: "f",
        word: "Niere"
      },
      %{
        gender: "m",
        word: "Öpfel"
      },
      %{
        gender: "u",
        word: "Iis"
      },
      %{
        gender: "f",
        word: "Wüsti"
      },
      %{
        gender: "m",
        word: "Aal"
      },
      %{
        gender: "m",
        word: "Chratzer"
      },
      %{
        gender: "m",
        word: "Hodä"
      },
      %{
        gender: "m",
        word: "Sack"
      },
      %{
        gender: "u",
        word: "Horn"
      },
      %{
        gender: "u",
        word: "Pulver"
      },
      %{
        gender: "u",
        word: "Puder"
      },
      %{
        gender: "u",
        word: "Mässer"
      },
      %{
        gender: "m",
        word: "Drogä"
      },
      %{
        gender: "m",
        word: "Busä"
      },
      %{
        gender: "m",
        word: "Liim"
      },
      %{
        gender: "m",
        word: "Bonker"
      },
      %{
        gender: "m",
        word: "Pflock"
      },
      %{
        gender: "m",
        word: "Bolzä"
      },
      %{
        gender: "m",
        word: "Pelz"
      },
      %{
        gender: "m",
        word: "Gloon"
      },
      %{
        gender: "f",
        word: "Spitze"
      },
      %{
        gender: "u",
        word: "Glänk"
      },
      %{
        gender: "u",
        word: "Ameisi"
      },
      %{
        gender: "m",
        word: "Bärg"
      },
      %{
        gender: "m",
        word: "Buuch"
      },
      %{
        gender: "m",
        word: "Baum"
      },
      %{
        gender: "m",
        word: "Moskel"
      },
      %{
        gender: "m",
        word: "Nabäl"
      },
      %{
        gender: "m",
        word: "Rompf"
      },
      %{
        gender: "u",
        word: "Monstär"
      },
      %{
        gender: "u",
        word: "Gaggi"
      },
      %{
        gender: "f",
        word: "Chotz"
      },
      %{
        gender: "f",
        word: "Ragete"
      },
      %{
        gender: "m",
        word: "Kiosk"
      },
      %{
        gender: "m",
        word: "Arsch"
      },
      %{
        gender: "u",
        word: "Baby"
      },
      %{
        gender: "f",
        word: "Hosä"
      },
      %{
        gender: "m",
        word: "Traum"
      },
      %{
        gender: "f",
        word: "Palme"
      },
      %{
        gender: "m",
        word: "Sex"
      },
      %{
        gender: "f",
        word: "Hoor"
      },
      %{
        gender: "u",
        word: "Weecee"
      },
      %{
        gender: "f",
        word: "Tomatä"
      },
      %{
        gender: "u",
        word: "Püree"
      },
      %{
        gender: "m",
        word: "Pfäffer"
      },
      %{
        gender: "u",
        word: "Schnetzel"
      },
      %{
        gender: "f",
        word: "Broschä"
      },
      %{
        gender: "f",
        word: "Katastrophä"
      },
      %{
        gender: "u",
        word: "Wasser"
      },
      %{
        gender: "f",
        word: "Liiche"
      },
      %{
        gender: "m",
        word: "Strahl"
      },
      %{
        gender: "m",
        word: "Puls"
      },
      %{
        gender: "f",
        word: "Risotto"
      },
      %{
        gender: "f",
        word: "Soppe"
      },
      %{
        gender: "m",
        word: "Wulst"
      },
      %{
        gender: "m",
        word: "Schwanz"
      },
      %{
        gender: "f",
        word: "Krüke"
      },
      %{
        gender: "m",
        word: "Zwärg"
      },
      %{
        gender: "f",
        word: "Ronzle"
      },
      %{
        gender: "m",
        word: "Fäger"
      }
    ]
  end

  def adjectives(:ch) do
    [
      %{
        m: "pulsierende",
        f: "pulsierendi",
        u: "pulsierendes"
      },
      %{
        m: "schräge",
        f: "schrägi",
        u: "schrägs"
      },
      %{
        m: "iiklemmte",
        f: "iiklemmti",
        u: "iiklemmts"
      },
      %{
        m: "saftige",
        f: "saftigi",
        u: "saftigs"
      }
    ]
  end
end
