defmodule JunikornEngine.Server do
  @moduledoc """
  The server.
  """

  use GenServer

  @impl true
  def init(init_arg) do
    {:ok, init_arg}
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, [], name: opts[:name] || __MODULE__)
  end

  def generate_name(pid) do
    GenServer.call(pid, {:generate_name})
  end

  def get_state(pid), do: GenServer.call(pid, {:get_state})

  # Server (callbacks)

  @impl true
  def handle_call({:generate_name}, _from, _state_data) do
    with name <- JunikornEngine.Name.generate(:de) do
      FrontendWeb.Endpoint.broadcast_from!(self(), "mainstage_de", "message", name)
      {:reply, {:ok, name}, name}
    end
  end

  def handle_call({:get_state}, _from, state_data) do
    {:reply, {:ok, state_data}, state_data}
  end
end
