defmodule JunikornEngine.NameGenerator do
  @moduledoc """
  This recursive task process creates periodically a new name. After a certain amount of time it creates a name and call himself.
  """
  use Task

  alias JunikornEngine.Server

  def start_link(locale) do
    Task.start_link(__MODULE__, :run, [locale])
  end

  @doc """
  Runs name creation task.
  """
  def run(locale) do
    receive do
    after
      3000 ->
        Server.generate_name(Server)
        run(locale)
    end
  end
end
