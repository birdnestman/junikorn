defmodule JunikornEngine.Name do
  @moduledoc """
  Handles full names. A name is a combination of words.
  """

  alias __MODULE__

  alias JunikornEngine.Word

  def join(args), do: join(args, Enum.count(args))

  def join(args, amount) when amount == 2 do
    %{name: "", words: args}
    |> concat(Enum.fetch!(args, 0), :secondlast)
    |> concat(Enum.fetch!(args, 1), :last)
  end

  def join(args, amount) when amount == 3 do
    %{name: "", words: args}
    |> concat(Enum.fetch!(args, 0))
    |> concat(Enum.fetch!(args, 1), :secondlast)
    |> concat(Enum.fetch!(args, 2), :last)
  end

  @doc """
  Concat a string
  """
  def concat(object, word), do: put_in(object.name, "#{object.name} #{word.word}")

  def concat(object, word, postition) when postition == :secondlast do
    case String.slice(word.word, -1..-1) do
      "e" -> put_in(object.name, "#{object.name} #{word.word}n")
      "d" -> put_in(object.name, "#{object.name} #{word.word}e")
      _ -> put_in(object.name, "#{object.name} #{word.word}")
    end
  end

  def concat(object, word, postition) when postition == :last do
    word = String.downcase(word.word)
    put_in(object.name, String.trim("#{object.name}#{word}"))
  end

  def generate(locale) do
    %{}
    |> Name.generate_random_words(locale)
    |> Name.join()
    |> Name.set_gender()
  end

  def generate_random_words(map \\ %{}, locale) do
    map
    |> Word.get_random(:noun, locale)
    |> Word.get_random(:adjective, locale)
  end

  def set_gender(words) do
    words
    |> Map.put(:gender, determine_gender(Map.get(words, :words)))
  end

  def determine_gender(words) do
    words
    |> List.last()
    |> Map.get(:gender)
  end
end
