FROM elixir:1.8.1-alpine

#Set environment variables and expose port
EXPOSE 4000
ENV REPLACE_OS_VARS=true \
    PORT=4000

RUN apk update
RUN apk upgrade
RUN apk add bash

WORKDIR /opt
#Copy and extract .tar.gz Release file from the previous stage
COPY junikorn_umbrella/ /opt/app/

ENTRYPOINT ["/opt/app/bin/junikorn_umbrella"]

CMD ["foreground"]
